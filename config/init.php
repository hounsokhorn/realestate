<?php
    include $_SERVER['DOCUMENT_ROOT'].'/../config/config.php';
    include LIB.'pdo2.php';
    
    // SMARTY
    require(LIB.'/vendor/smarty/Smarty.class.php');
    $smarty = new Smarty();
    // Config Smarty
    //$smarty->force_compile = true;
    //$smarty->compile_check = true;
    $smarty->setTemplateDir(VIEW);
    $smarty->setCompileDir(MVC_ROOT.'view/smarty/templates_c');
    $smarty->setCacheDir(MVC_ROOT.'view/smarty/cache');
    $smarty->setConfigDir(MVC_ROOT.'view/smarty/configs');
    //$smarty->setCompileCheck(false);
    //$smarty->caching = true;
    //$smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    //$smarty->setCaching(Smarty::CACHING_LIFETIME_SAVED);
    //$smarty->clearAllCache();
?>
