<?php
/************************************** NE PAS TOUCHER **********************************************/
/**/
/**/ include('connection.php');
/**/
/**/ // Chemins à utiliser pour accéder aux vues/modeles/librairies
/**/ define('MVC_ROOT',    $_SERVER['DOCUMENT_ROOT'].'/../');
/**/ define('CONTROLLER_ROOT',    MVC_ROOT.'controller/'); 

     
     if(!isset($_GET['module'])) $_GET['module'] = 'page'  ;
     if(!isset($_GET['action'])) $_GET['action'] = 'index';
     
     $module = $_GET['module'];
     $action = $_GET['action'];

     $action = str_replace('-', '_', $action);  

/**/ define('MODEL', 			MVC_ROOT.'model/');
/**/ define('VIEW',    		    MVC_ROOT.'view/'.$module.'/');
/**/ define('VIEW_GLOBAL', 	    MVC_ROOT.'view/');
/**/ define('VIEW_REUSABLE', 	MVC_ROOT.'view/reusable/');
/**/ define('CONTROLLER',    	MVC_ROOT.'controller/'.$module.'/');
     define('CONTROLLER_GLOBAL', 	MVC_ROOT.'controller/');
/**/ define('LIB',    			MVC_ROOT.'lib/');
/**/ define('CONFIG',    		MVC_ROOT.'config/');
/**/ define('IMG',    			MVC_ROOT.'webroot/style/img/');
/**/ define('WWW',    			MVC_ROOT.'webroot/');
/**/ 

/**/
/*********************************** FIN NE PAS TOUCHER **********************************************/

?>
