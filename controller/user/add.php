<?php
require_once MODEL.'user.php';
require_once CONTROLLER_GLOBAL.'global.php';
require_once WWW.'upload/ImageManipulator.php';

$roleoptions = readRoleName();
$rolevalues = readRoleId();

$smarty->assign('roles', $roleoptions);
$smarty->assign('values', $rolevalues);

if($_POST){
    
    if ($_FILES['photo']['error'] > 0) {
        echo "Error: " . $_FILES['photo']['error'] . "<br />";
    } else {
        // array of valid extensions
        $validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
        // get extension of the uploaded file
        $fileExtension = strrchr($_FILES['photo']['name'], ".");
        // check if file Extension is on the list of allowed ones
        // Crop
        if (in_array($fileExtension, $validExtensions)) {
            $newNamePrefix = time() . '_';
            $manipulator = new ImageManipulator($_FILES['photo']['tmp_name']);
            // resizing to 200x200
            $newImage = $manipulator->resample(200, 200);            
            // saving file to uploads folder
            $manipulator->save('images/upload/' . $newNamePrefix . $_FILES['photo']['name']);
            
            $_POST['photo'] = $newNamePrefix . $_FILES['photo']['name'];
            create($_POST);
            redirect('/'.$module);
//        } 
        //Center Crop
//        if (in_array($fileExtension, $validExtensions)) {
//            $newNamePrefix = time() . '_';
//            $manipulator = new ImageManipulator($_FILES['photo']['tmp_name']);
//            $width  = $manipulator->getWidth();
//            $height = $manipulator->getHeight();
//            $centreX = round($width / 2);
//            $centreY = round($height / 2);
//            // our dimensions will be 200x130
//            $x1 = $centreX - 100; // 200 / 2
//            $y1 = $centreY - 65; // 130 / 2
//
//            $x2 = $centreX + 100; // 200 / 2
//            $y2 = $centreY + 65; // 130 / 2
//
//            // center cropping to 200x130
//            $newImage = $manipulator->crop($x1, $y1, $x2, $y2);
//            // saving file to uploads folder
//            $manipulator->save('images/upload/' . $newNamePrefix . $_FILES['photo']['name']);
//            echo 'Done ...';
        }else {
            $smarty->assign('error_upload','You must upload an images');
        }
    }
}

$smarty->display(VIEW.'add.tpl');
?>
