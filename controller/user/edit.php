<?php
require_once MODEL.'user.php';
require_once CONTROLLER_GLOBAL.'global.php';
require_once WWW.'upload/ImageManipulator.php';

if(isset($_GET['id'])){
    $readById = readById($_GET['id']);
    $smarty->assign('readById', $readById);
}
$roleoptions = readRoleName();
$rolevalues = readRoleId();

$smarty->assign('roles', $roleoptions);
$smarty->assign('values', $rolevalues);

if($_POST){
    if ($_FILES['photo']['error'] > 0) {
        echo "Error: " . $_FILES['photo']['error'] . "<br />";
    } else {
        // array of valid extensions
        $validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
        // get extension of the uploaded file
        $fileExtension = strrchr($_FILES['photo']['name'], ".");
        // check if file Extension is on the list of allowed ones
        // Crop
        if (in_array($fileExtension, $validExtensions)) {
            $newNamePrefix = time() . '_';
            $manipulator = new ImageManipulator($_FILES['photo']['tmp_name']);
            // resizing to 200x200
            $newImage = $manipulator->resample(200, 200);            
            // saving file to uploads folder
            $manipulator->save('images/upload/' . $newNamePrefix . $_FILES['photo']['name']);
            
            $_POST['photo'] = $newNamePrefix . $_FILES['photo']['name'];
            update($_POST);
            redirect('/'.$module);
        }else {
            $smarty->assign('error_upload','You must upload an images');
        }
    }
}
$smarty->display(VIEW.'edit.tpl');
?>

