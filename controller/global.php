<?php
    function redirect($url,$permanent = false) {
        if($permanent) {
                header('HTTP/1.1 301 Moved Permanently');
        }
        header('Location: '.$url);
        exit();
    }
//    function redirect($url, $permanent = false) {
//        if($permanent) {
//                header('HTTP/1.1 301 Moved Permanently');
//        }
//        header('Location: '.$url);
//        exit();
//    }
// function listImages($dir){
//    $images = array();
//    // Si c'est bien un dossier, on récupère les jpg qui sont dedans
//    if (is_dir($dir)){
//    	$scandir = scandir($dir);
//    	foreach ($scandir as $image){
//    		$extension = strtolower(pathinfo($dir.$image, PATHINFO_EXTENSION));
//    		if ($extension == 'jpg')
//    			$images[] = $image;
//    	}
//    }
//    return $images;
// }
// function countImg($folder, $filetype = '*.jpg'){
//    $files = glob($folder.$filetype);
//    return count($files);
// }
 function debug($data = null){
    echo '<div class="debug">';
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    echo '</div>';
 }
?>
