/*
SQLyog Enterprise - MySQL GUI v6.5
MySQL - 5.5.24-log : Database - freeland
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

create database if not exists `freeland`;

USE `freeland`;

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `admin_create` tinyint(1) DEFAULT '1',
  `admin_login` tinyint(1) DEFAULT '1',
  `active` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `add` int(11) DEFAULT NULL,
  `edit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`admin_create`,`admin_login`,`active`,`created`,`modified`,`add`,`edit`) values (1,'Developers',1,1,1,'2012-09-11 18:48:03','2012-10-10 09:58:05',NULL,NULL),(2,'Administrators',1,1,1,'2012-09-11 18:48:13','2012-09-11 18:48:13',NULL,NULL),(3,'Users',1,1,1,'2012-10-23 09:59:53','2012-10-23 09:59:53',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `last_login` datetime DEFAULT NULL,
  `role_id` smallint(4) DEFAULT '3',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `BY_EMAIL` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`dob`,`address`,`photo`,`phone`,`password`,`email`,`active`,`last_login`,`role_id`,`created`,`modified`) values (5,'ty sochea','1988-04-09','Phnom Penh',NULL,'015 999283','tsc','me.sochea@gmail.com',1,NULL,3,'2014-07-16 00:00:00',NULL),(8,'test1','2014-07-15',NULL,NULL,NULL,'tsc','test@gmail.com',1,NULL,3,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
