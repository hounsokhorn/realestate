<?php 
    function read(){
        $pdo = PDO2::getInstance();
        $sql = $pdo->prepare("SELECT * FROM users");
	$sql->execute();
        while($r = $sql->fetch(PDO::FETCH_ASSOC)){
            $data[]=$r;
	}
	return $data;
    }        
    function readById($id){
            $pdo = PDO2::getInstance();
            $sql="SELECT * FROM users WHERE id = :id";
            $q = $pdo->prepare($sql);
            $q->execute(array(':id'=>$id));
            $data = $q->fetch(PDO::FETCH_ASSOC);
            return $data;	
    }
    function create($post){
        $pdo = PDO2::getInstance();
            
        $keys = array_keys($post);
        $fields = '`'.implode('`, `',$keys).'`';
        $values = ':'.implode(', :',$keys);
        $q = $pdo->prepare("
                    INSERT INTO `users`
                    ($fields)
                    VALUES
                    ($values);
            ");
        $q->bindParam('name', $post['name']);
        $q->bindParam('email', $post['email']);
        $q->bindParam('password', md5($post['password']));
        $q->bindParam('dob', $post['dob']);
        $q->bindParam('address', $post['address']);
        $q->bindParam('phone', $post['phone']);
        $q->bindParam('role_id', $post['role_id']);
        $q->bindParam('photo', $post['photo']);
        $q->execute();
        
        //user for execute second statement if have
        //$q->closeCursor();        
    }    
    function update($post){
            $pdo = PDO2::getInstance();
            
            $q = $pdo->prepare("
		UPDATE `users`
		SET `name` = :name, `email` = :email, `password` = :password, `dob` = :dob,
                    `address` = :address, `phone` = :phone, `role_id` = :role_id, `photo` = :photo
                    WHERE `id` = :id
                ");
            $q->bindParam('name', $post['name']);
            $q->bindParam('email', $post['email']);
            $q->bindParam(':password', $post['password']);
            $q->bindParam('dob', $post['dob']);
            $q->bindParam('address', $post['address']);
            $q->bindParam('phone', $post['phone']);
            $q->bindParam('role_id', $post['role_id']);
            $q->bindParam('photo', $post['photo']);
            $q->bindParam('id', $post['id']);
            $q->execute();
            
            //return true;
    }
    function delete($id){
            $pdo = PDO2::getInstance();
            $sql="DELETE FROM users WHERE id=:id";
            $q = $pdo->prepare($sql);
            $q->execute(array(':id'=>$id));
            //return true;	
    }
    
    ////////////////////////
    function readRoleId(){
        $pdo = PDO2::getInstance();
        $sql  = $pdo->prepare("SELECT id FROM roles");
        $sql->execute();
        while($r = $sql->fetch(PDO::FETCH_COLUMN)){
            $data[]=$r;
	}
	return $data;
    }
    function readRoleName(){
        $pdo = PDO2::getInstance();
        $sql  = $pdo->prepare("SELECT name FROM roles");
        $sql->execute();
        while($r = $sql->fetch(PDO::FETCH_COLUMN)){
            $data[]=$r;
	}
	return $data;
    }
?>

