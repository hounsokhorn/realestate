<?php
    
    function search($name, $table){
        try{
            $q = "SELECT * FROM $table WHERE name LIKE ?";
            $stmt = $this->conn->prepare($q);
            $stmt->execute(array("%$name%"));
        }catch(PDOExcept $e){
            throw new Exception("ERROR:". $e->getMessage()); 
        }
        if(!$stmt->rowCount()){
          return false; #this will return false if data isn't found. 
        }
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
?>

