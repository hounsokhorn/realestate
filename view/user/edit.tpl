{include file="../element/admin_header.tpl"}    
        <br>
        <div class="col-md-6">
        <div role="box-body">
        <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data"> 
            <div class="form-group">
                <label for="name" >Name</label>
                <input type="text" class="form-control" name="name" placeholder="User Name" required="required" value="{$readById.name}">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <input type="email" class="form-control" name="email" placeholder="me.sochea@gmail.com" required="required" value="{$readById.email}">
                </div>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="*******" required="required" value="{$readById.password}">
            </div>
            <div class="form-group">
                <label for="BirthDate">BirthDate</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" name="dob" placeholder="1988-04-30" value="{$readById.dob}">
                </div>
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <div class="input-group">
                    <span class="input-group-addon">@</span>
                    <input type="text" class="form-control" name="address" placeholder="Phnom Penh" value="{$readById.address}">
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <input type="tel" class="form-control" name="phone" placeholder="015 999 283" value="{$readById.phone}">
                </div>
            </div>
            <div class="form-group">
                <label for="select_role">Role</label>
                <select name="role_id" class="form-control">
                    {html_options values=$values output=$roles selected="{$readById.role_id}"}
                </select>
            </div>
            <div class="form-group" >
                <label for="photo">Profile Photo</label>
                <input name="photo" type="file">
                <p style="color: red;font-style: italic;">{if isset($error_upload)}{$error_upload}{/if}</p>
            </div>
            <input type="hidden" name="id" value="{$readById.id}" />
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default btn-lg btn-block">Edit user</button>
                </div>
            </div>
        </form>
        </div>
        </div>
{include file="../element/admin_footer.tpl"}