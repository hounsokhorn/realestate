{include file="../element/admin_header.tpl"}

<table class="table table-bordered table-hover dataTable">
    <thead>
        <tr role="row">
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Photo</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Online</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    {foreach $users as $i => $user}
        <tr>
            <td>{$i+1}</td>
            <td>{$user.name}</td>
            <td>{$user.address}</td>
            <td>{$user.photo}</td>
            <td>{$user.phone}</td>
            <td>{$user.email}</td>
            <td>{$user.active}</td>
            <td>
                <a class="btn btn-default btn-xs" href="/user/edit/{$user.id}" role="button">Edit</a>
                <a class="btn btn-default btn-xs" href="/user/delete/{$user.id}" role="button" onclick="return confirm('Really delete?');">Delete</a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
<hr>
<a class="btn btn-default" href="/{$smarty.get.module}/add" role="button">Add new</a>

{include file="../element/admin_footer.tpl"}