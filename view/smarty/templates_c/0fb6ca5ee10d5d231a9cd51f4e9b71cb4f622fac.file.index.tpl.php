<?php /* Smarty version Smarty-3.1.14, created on 2014-07-24 02:24:58
         compiled from "D:\Dropbox\dev\realestate\view\user\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2061053ce0c2d893128-75327479%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0fb6ca5ee10d5d231a9cd51f4e9b71cb4f622fac' => 
    array (
      0 => 'D:\\Dropbox\\dev\\realestate\\view\\user\\index.tpl',
      1 => 1406034848,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2061053ce0c2d893128-75327479',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53ce0c2d979c97_24393163',
  'variables' => 
  array (
    'users' => 0,
    'i' => 0,
    'user' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ce0c2d979c97_24393163')) {function content_53ce0c2d979c97_24393163($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("../element/admin_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<table class="table table-bordered table-hover dataTable">
    <thead>
        <tr role="row">
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Photo</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Online</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
 $_smarty_tpl->tpl_vars['i']->value = $_smarty_tpl->tpl_vars['user']->key;
?>
        <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['i']->value+1;?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['user']->value['name'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['user']->value['address'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['user']->value['photo'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['user']->value['phone'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['user']->value['email'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['user']->value['active'];?>
</td>
            <td>
                <a class="btn btn-default btn-xs" href="/user/edit/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" role="button">Edit</a>
                <a class="btn btn-default btn-xs" href="/user/delete/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" role="button" onclick="return confirm('Really delete?');">Delete</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<hr>
<a class="btn btn-default" href="/<?php echo $_GET['module'];?>
/add" role="button">Add new</a>

<?php echo $_smarty_tpl->getSubTemplate ("../element/admin_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>