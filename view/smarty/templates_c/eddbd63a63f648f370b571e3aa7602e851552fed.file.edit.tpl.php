<?php /* Smarty version Smarty-3.1.14, created on 2014-07-24 02:25:14
         compiled from "D:\Dropbox\dev\realestate\view\user\edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2199953ce1451dbe212-58688198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eddbd63a63f648f370b571e3aa7602e851552fed' => 
    array (
      0 => 'D:\\Dropbox\\dev\\realestate\\view\\user\\edit.tpl',
      1 => 1406037133,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2199953ce1451dbe212-58688198',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53ce1451e3f896_67264404',
  'variables' => 
  array (
    'readById' => 0,
    'values' => 0,
    'roles' => 0,
    'error_upload' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53ce1451e3f896_67264404')) {function content_53ce1451e3f896_67264404($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'D:\\Dropbox\\dev\\realestate\\lib\\vendor\\smarty\\plugins\\function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("../element/admin_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
    
        <br>
        <div class="col-md-6">
        <div role="box-body">
        <form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data"> 
            <div class="form-group">
                <label for="name" >Name</label>
                <input type="text" class="form-control" name="name" placeholder="User Name" required="required" value="<?php echo $_smarty_tpl->tpl_vars['readById']->value['name'];?>
">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <input type="email" class="form-control" name="email" placeholder="me.sochea@gmail.com" required="required" value="<?php echo $_smarty_tpl->tpl_vars['readById']->value['email'];?>
">
                </div>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="*******" required="required" value="<?php echo $_smarty_tpl->tpl_vars['readById']->value['password'];?>
">
            </div>
            <div class="form-group">
                <label for="BirthDate">BirthDate</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" name="dob" placeholder="1988-04-30" value="<?php echo $_smarty_tpl->tpl_vars['readById']->value['dob'];?>
">
                </div>
            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <div class="input-group">
                    <span class="input-group-addon">@</span>
                    <input type="text" class="form-control" name="address" placeholder="Phnom Penh" value="<?php echo $_smarty_tpl->tpl_vars['readById']->value['address'];?>
">
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-phone"></i>
                    </div>
                    <input type="tel" class="form-control" name="phone" placeholder="015 999 283" value="<?php echo $_smarty_tpl->tpl_vars['readById']->value['phone'];?>
">
                </div>
            </div>
            <div class="form-group">
                <label for="select_role">Role</label>
                <select name="role_id" class="form-control">
                    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['values']->value,'output'=>$_smarty_tpl->tpl_vars['roles']->value,'selected'=>((string)$_smarty_tpl->tpl_vars['readById']->value['role_id'])),$_smarty_tpl);?>

                </select>
            </div>
            <div class="form-group" >
                <label for="photo">Profile Photo</label>
                <input name="photo" type="file">
                <p style="color: red;font-style: italic;"><?php if (isset($_smarty_tpl->tpl_vars['error_upload']->value)){?><?php echo $_smarty_tpl->tpl_vars['error_upload']->value;?>
<?php }?></p>
            </div>
            <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['readById']->value['id'];?>
" />
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default btn-lg btn-block">Edit user</button>
                </div>
            </div>
        </form>
        </div>
        </div>
<?php echo $_smarty_tpl->getSubTemplate ("../element/admin_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>